<?php

// Auto backup chat Instagram
// Credit: Alfian Ananda Putra

date_default_timezone_set("Asia/Jakarta");
error_reporting(0);

function getCSRF(){
  $fgc    =   file_get_contents("https://www.instagram.com");
  $explode    =   explode('"csrf_token":"',$fgc);
  $explode    =   explode('"',$explode[1]);
  return $explode[0];
}

// Membuat device id Android
function generateDeviceId(){
  $megaRandomHash = md5(number_format(microtime(true), 7, '', ''));
  return 'android-'.substr($megaRandomHash, 16);
}

// Membuat UUID
function generateUUID($keepDashes = true){
  $uuid = sprintf(
    '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    mt_rand(0, 0xffff),
    mt_rand(0, 0xffff),
    mt_rand(0, 0xffff),
    mt_rand(0, 0x0fff) | 0x4000,
    mt_rand(0, 0x3fff) | 0x8000,
    mt_rand(0, 0xffff),
    mt_rand(0, 0xffff),
    mt_rand(0, 0xffff)
  );
  return $keepDashes ? $uuid : str_replace('-', '', $uuid);
}

// Membuat signed_body untuk UA : Instagram 24.0.0.12.201 Android
function hookGenerate($hook){
  return 'ig_sig_key_version=4&signed_body=' . hash_hmac('sha256', $hook, '5bd86df31dc496a3a9fddb751515cc7602bdad357d085ac3c5531e18384068b4') . '.' . urlencode($hook);
}

// Fungsi request untuk mengirim data
function request($url,$hookdata,$cookie,$method='GET'){
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, "https://i.instagram.com/api".$url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HEADER, 1);
  if($method == 'POST'){
    curl_setopt($ch, CURLOPT_POSTFIELDS, $hookdata);
    curl_setopt($ch, CURLOPT_POST, 1);
  }else{
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
  }

  $headers = array();
  $headers[] = "Accept: */*";
  $headers[] = "Content-Type: application/x-www-form-urlencoded";
  $headers[] = 'Cookie2: _ENV["Version=1"]';
  $headers[] = "Accept-Language: en-US";
  $headers[] = "User-Agent: Instagram 24.0.0.12.201 Android (28/9; 320dpi; 720x1280; samsung; SM-J530Y; j5y17ltedx; samsungexynos7870; in_ID;)";
  $headers[] = "Host: i.instagram.com";
  if($cookie !== "0"){
    $headers[] = "Cookie: ".$cookie;
  }
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

  $result = curl_exec($ch);
  $httpcode  = curl_getinfo($ch);
  $header    = substr($result, 0, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
  $body      = substr($result, curl_getinfo($ch, CURLINFO_HEADER_SIZE));

  if(curl_errno($ch)){
    echo 'Error:' . curl_error($ch);
  }
  curl_close ($ch);
  return array($header, $body, $httpcode,$result,$url,$hookdata); // body itu response body
}

echo "   _    _ _ _             _____  __  __
 | |  | | | |           |  __ \|  \/  |
 | |  | | | |_ _ __ __ _| |  | | \  / |
 | |  | | | __| '__/ _` | |  | | |\/| |
 | |__| | | |_| | | (_| | |__| | |  | |
  \____/|_|\__|_|  \__,_|_____/|_|  |_|


© Pianjammalam 2020

-----------------------------------------------------------
";

echo 'Selamat datang di UltraDM V1 by @pianjammalam.
Punya temen suka unsend dm? Lagi deket doi, tapi doi sering unsend dm? kepo ama dm yang udah di delete? Saatnya pakai UltraDM!

Fitur sementara:
- Backup Text
- Backup Story in DM (raven media)
- Backup Gambar
- Backup Love

Perlu diingat bahwa bot ini auto read dm kalian!

Username: @';

$usernameIg = trim(fgets(STDIN));

echo 'Password: ';

$passwordIg = trim(fgets(STDIN));

if(!empty($usernameIg) and !empty($passwordIg)){
  $genDevId = generateDeviceId();
  $tryLogin = request('/v1/accounts/login/',hookGenerate('{"phone_id":"'.generateUUID().'","_csrftoken":"'.getCSRF().'","username":"'.$usernameIg.'","adid":"'.generateUUID().'","guid":"'.generateUUID().'","device_id":"'.$genDevId.'","password":"'.$passwordIg.'","login_attempt_count":"0"}'),0,"POST");
  if(!empty(json_decode($tryLogin[1],true)['logged_in_user']['pk'])){
    $uid = json_decode($tryLogin[1],true)['logged_in_user']['pk'];
    preg_match_all('%set-cookie: (.*?);%',$tryLogin[0],$d);$cookieInstagram = '';
    for($o=0;$o<count($d[0]);$o++){$cookieInstagram.=$d[1][$o].";";}
    // Menyimpan cookie ke file cookie.txt
    $fwriteFunc = fopen('cookie.txt', 'w');
    fwrite($fwriteFunc, $cookieInstagram);
    fclose($fwriteFunc);

    while(true){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://i.instagram.com/api/v1/direct_v2/inbox/?visual_message_return_type=unseen&thread_message_limit=10&persistentBadging=true&limit=2&fetch_reason=manual_refresh');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

      $headers = array();
      $headers[] = 'X-Ig-Connection-Type: WIFI';
      $headers[] = 'User-Agent: Instagram 35.0.0.20.96 Android (21/5.0; 480dpi; 1080x1920; asus; ASUS_Z00AD; Z00A_1; mofd_v1; in_ID; 95414347)';
      $headers[] = 'Accept-Language: id-ID, en-US';
      $headers[] = "Cookie: ".$cookieInstagram;
      $headers[] = 'Host: i.instagram.com';
      $headers[] = 'X-Fb-Http-Engine: Liger';
      $headers[] = 'Content-Type: application/x-www-form-urlencoded';
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);
      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
      }
      curl_close($ch);
      foreach (json_decode($result, true)['inbox']['threads'] as $key => $value) {
        if(json_decode($result, true)['inbox']['threads'][$key]['read_state'] == '1' and empty(json_decode($result, true)['inbox']['threads'][$key]['users'][1])){
          echo '['.date("d-M-Y").' '.date("h:i:s").'] @'.json_decode($result, true)['inbox']['threads'][$key]['thread_title'].' Mengirim pesan baru
';
          if(json_decode($result, true)['inbox']['threads'][$key]['items'][0]['item_type']  ==  'text'){
            //Ini bentuk text
            $timeReal = date('m/d/Y H:i:s', substr(json_decode($result, true)['inbox']['threads'][$key]['items'][0]['timestamp'],0,10));
            $formatToSave = '['.$timeReal.'] '.json_decode($result, true)['inbox']['threads'][$key]['items'][0]['text'].'
';
            $file = fopen("BACKUPDM-".json_decode($result, true)['inbox']['threads'][$key]['thread_title'].".txt", "a");
            fwrite($file, $formatToSave);
            fclose($file);
            echo '['.date("d-M-Y").' '.date("h:i:s").'] @'.json_decode($result, true)['inbox']['threads'][$key]['thread_title'].': '.json_decode($result, true)['inbox']['threads'][$key]['items'][0]['text'].'
';
          }elseif(json_decode($result, true)['inbox']['threads'][$key]['items'][0]['item_type']  ==  'raven_media'){
            //Ini bentuk story
            $timeReal = date('m/d/Y H:i:s', substr(json_decode($result, true)['inbox']['threads'][$key]['items'][0]['timestamp'],0,10));
            $formatToSave = '['.$timeReal.'] (RAVEN_MEDIA)'.json_decode($result, true)['inbox']['threads'][$key]['items'][0]['raven_media']['image_versions2']['candidates'][0]['url'].'
';
            $file = fopen("BACKUPDM-".json_decode($result, true)['inbox']['threads'][$key]['thread_title'].".txt", "a");
            fwrite($file, $formatToSave);
            fclose($file);
            echo '['.date("d-M-Y").' '.date("h:i:s").'] @'.json_decode($result, true)['inbox']['threads'][$key]['thread_title'].': (RAVEN_MEDIA) '.json_decode($result, true)['inbox']['threads'][$key]['items'][0]['raven_media']['image_versions2']['candidates'][0]['url'].'
';
            $createClientContex = rand(6651407307554442964,9999999999999999999);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://i.instagram.com/api/v1/direct_v2/visual_threads/'.json_decode($result, true)['inbox']['threads'][$key]['thread_id'].'/item_seen/');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "signed_body=e6b416ce331712caf7be7cbacddf79eea317674f0d78cba59796aa.%7B%22_csrftoken%22%3A%22K".rand(1111,9999)."c".rand(1111,9999)."kt".rand(1111,9999)."FLrA".rand(1111,9999)."Jj".rand(1111,9999)."5y%22%2C%22_uid%22%3A%22".$uid."%22%2C%22_uuid%22%3A%22g".rand(1111,9999)."76S-".rand(1111,9999)."-45h3-".rand(1111,9999)."-c8c7948f".rand(1111,9999)."%22%2C%22original_message_client_context%22%3A%22".$createClientContex."%22%2C%22item_ids%22%3A%22%5B".json_decode($result, true)['inbox']['threads'][$key]['items'][0]['item_id']."%5D%22%2C%22target_item_type%22%3A%22raven_media%22%7D&ig_sig_key_version=4");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

            $headers = array();
            $headers[] = 'X-Ig-Connection-Type: WIFI';
            $headers[] = 'User-Agent: Instagram 35.0.0.20.96 Android (21/5.0; 480dpi; 1080x1920; asus; ASUS_Z00AD; Z00A_1; mofd_v1; in_ID; 95414347)';
            $headers[] = 'Accept-Language: id-ID, en-US';
            $headers[] = "Cookie: ".$cookieInstagram;
            $headers[] = 'Host: i.instagram.com';
            $headers[] = 'X-Fb-Http-Engine: Liger';
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $resultRaven = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
          }elseif(json_decode($result, true)['inbox']['threads'][$key]['items'][0]['item_type']  ==  'media'){
            //Ini bentuk gambar
            $timeReal = date('m/d/Y H:i:s', substr(json_decode($result, true)['inbox']['threads'][$key]['items'][0]['timestamp'],0,10));
            $formatToSave = '['.$timeReal.'] (GAMBAR)'.json_decode($result, true)['inbox']['threads'][$key]['items'][0]['media']['image_versions2']['candidates'][0]['url'].'
';
            $file = fopen("BACKUPDM-".json_decode($result, true)['inbox']['threads'][$key]['thread_title'].".txt", "a");
            fwrite($file, $formatToSave);
            fclose($file);
            echo '['.date("d-M-Y").' '.date("h:i:s").'] @'.json_decode($result, true)['inbox']['threads'][$key]['thread_title'].': (GAMBAR) '.json_decode($result, true)['inbox']['threads'][$key]['items'][0]['media']['image_versions2']['candidates'][0]['url'].'
';
          }elseif(json_decode($result, true)['inbox']['threads'][$key]['items'][0]['item_type']  ==  'like'){
            //Ini bentuk love
            $timeReal = date('m/d/Y H:i:s', substr(json_decode($result, true)['inbox']['threads'][$key]['items'][0]['timestamp'],0,10));
            $formatToSave = '['.$timeReal.'] Mengirim Love untuk anda
';
            $file = fopen("BACKUPDM-".json_decode($result, true)['inbox']['threads'][$key]['thread_title'].".txt", "a");
            fwrite($file, $formatToSave);
            fclose($file);
            echo '['.date("d-M-Y").' '.date("h:i:s").'] @'.json_decode($result, true)['inbox']['threads'][$key]['thread_title'].': Mengirim Love untuk anda
';
          }else{
            echo '['.date("d-M-Y").' '.date("h:i:s").'] @'.json_decode($result, true)['inbox']['threads'][$key]['thread_title'].' Mengirim pesan yang tidak dikenali formatnya
  '.print_r(json_decode($result, true)['inbox']['threads'][$key]['items'][0]);
          }
          $createClientContex = rand(6651407307554442964,9999999999999999999);

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, 'https://i.instagram.com/api/v1/direct_v2/threads/'.json_decode($result, true)['inbox']['threads'][$key]['thread_id'].'/items/'.json_decode($result, true)['inbox']['threads'][$key]['items'][0]['item_id'].'/seen/');
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, "thread_id=".json_decode($result, true)['inbox']['threads'][$key]['thread_id']."&action=mark_seen&client_context=".$createClientContex."&_csrftoken=K".rand(1111,9999)."c".rand(1111,9999)."kt".rand(1111,9999)."FLrA".rand(1111,9999)."Jj".rand(1111,9999)."5y&_uuid=g".rand(1111,9999)."76S-".rand(1111,9999)."-45h3-".rand(1111,9999)."-c8c7948f".rand(1111,9999)."&offline_threading_id=".$createClientContex);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

          $headers = array();
          $headers[] = 'X-Ig-Connection-Type: WIFI';
          $headers[] = 'User-Agent: Instagram 35.0.0.20.96 Android (21/5.0; 480dpi; 1080x1920; asus; ASUS_Z00AD; Z00A_1; mofd_v1; in_ID; 95414347)';
          $headers[] = 'Accept-Language: id-ID, en-US';
          $headers[] = "Cookie: ".$cookieInstagram;
          $headers[] = 'Host: i.instagram.com';
          $headers[] = 'X-Fb-Http-Engine: Liger';
          $headers[] = 'Content-Type: application/x-www-form-urlencoded';
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

          $resultAkhir = curl_exec($ch);
          if (curl_errno($ch)) {
              echo 'Error:' . curl_error($ch);
          }
          curl_close($ch);

        }else{
          echo '['.date("d-M-Y").' '.date("h:i:s").'] Tidak ditemukan pesan baru
';
        }
      }
      sleep(1);
    }
  }else{
    echo "
Ada yang salah dengan akunmu!";
  }
}else{
  echo "
Silahkan masukkan username dan password Instagram Anda!";
}
